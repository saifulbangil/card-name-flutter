import 'package:flutter/widgets.dart';

class MyFlutterApp {
  MyFlutterApp._();

  static const _kFontFam = 'MyFlutterApp';

  static const IconData phone_call = const IconData(0xe800, fontFamily: _kFontFam);
  static const IconData mail = const IconData(0xe801, fontFamily: _kFontFam);
}
